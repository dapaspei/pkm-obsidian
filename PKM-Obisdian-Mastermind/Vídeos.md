# Vídeos formativos

Autor::Alex
Versión:: 2022.06.19

Relación de vídeos de referencia capturados en este grupo Telegram:

Vídeos Canal Emowe: https://emowe.com/cerebro-digital/curso-tutorial-de-obsidian/
- 1 - Cómo empezar con Obsidian : https://www.youtube.com/watch?v=64pI_dKYZOg&list=PLWUX-KZsnKXSKOjd4WIbqB5GsHORz88JS
- 2  - Cómo configurar Obsidian : https://www.youtube.com/watch?v=YKKSgfZwA5s
- 3 - Cómo editar con Markdown : https://www.youtube.com/watch?v=q7snzrbARL4&t=1s
- 4 - Cómo hacer Copias de Seguridad : https://youtu.be/DfL6St7rbYM
- 5 - Más Foco en mi Vida con Obsidian : https://youtu.be/vhIp-1Ywjow?list=PLWUX-KZsnKXSKOjd4WIbqB5GsHORz88JS
- 6  - Cómo crear plantillas con Obsidian : https://youtu.be/SFh0-VxPUTQ?list=PLWUX-KZsnKXSKOjd4WIbqB5GsHORz88JS
- 7  - Gestión de tareas y proyectos : https://youtu.be/56CUYNV4CPY?list=PLWUX-KZsnKXSKOjd4WIbqB5GsHORz88JS
- 8 - Cómo tomar notas con Zettelkasten y Obsidian : https://youtu.be/hGd70QqTkCM?list=PLWUX-KZsnKXRfo53cET5Uk7Z0eU8AKaGq
- 9 - Cómo hacer mapas mentales gratis, fácil y rápido con Obsidian : https://youtu.be/IoKRt2-FLRk?list=PLWUX-KZsnKXSKOjd4WIbqB5GsHORz88JS
- 10 - Cómo crear tarjetas ANKI de forma rápida y gratis : https://youtu.be/UjrH4-z7qXY?list=PLWUX-KZsnKXSKOjd4WIbqB5GsHORz88JS
- 11 - Cómo pasar automáticamente notas y subrayados de Kindle a notas en texto plano : https://youtu.be/TVEjEWch8Ic?list=PLWUX-KZsnKXSKOjd4WIbqB5GsHORz88JS
- 12 - Cómo referenciar bloques o crear transclusión en Obsidian : https://youtu.be/fewZDCf4YnI?list=PLWUX-KZsnKXSKOjd4WIbqB5GsHORz88JS
- 13 - Cómo configurar el vault o bóveda de Obsidian : https://youtu.be/0N05ak-TD4M?list=PLWUX-KZsnKXSKOjd4WIbqB5GsHORz88JS
- 14 - Control de hábitos en Obsidian : https://youtu.be/zxrzrFqMqbY?list=PLWUX-KZsnKXSKOjd4WIbqB5GsHORz88JS
- 15 - Cómo convertir subrayados y anotaciones PDF a Markdown : https://youtu.be/hWnhSa0Ql6M?list=PLWUX-KZsnKXSKOjd4WIbqB5GsHORz88JS
- 16 - Cómo descargar el contenido de una página web y pasarlo a Markdown : https://youtu.be/of2YUdvQm-I?list=PLWUX-KZsnKXSKOjd4WIbqB5GsHORz88JS
- 17 - Cómo instalar Obsidian en móvil y tablet : https://youtu.be/IQFYj2CS8Cs?list=PLWUX-KZsnKXSKOjd4WIbqB5GsHORz88JS
- 18 - Cómo sincronizar y clonar vaults en Obsidian : https://youtu.be/yyK378zW_ac?list=PLWUX-KZsnKXSKOjd4WIbqB5GsHORz88JS
- 19 - Cómo poner texto en fotos y vídeos para convertir en historias : https://youtu.be/ewiob-WIaWU?list=PLWUX-KZsnKXSKOjd4WIbqB5GsHORz88JS
- 20 - Cómo hacer tableros Kanban en Obsidian : https://youtu.be/iULpaAJvKdc?list=PLWUX-KZsnKXSKOjd4WIbqB5GsHORz88JS
- 21 - Cómo encriptar notas en Obsidian : https://youtu.be/MPp1S1PpiKI?list=PLWUX-KZsnKXSKOjd4WIbqB5GsHORz88JS
- 22 - Cómo pasar de Wikilinks a Markdown links y viceversa : https://youtu.be/Fifr3oL3DcM?list=PLWUX-KZsnKXSKOjd4WIbqB5GsHORz88JS

Vídeos Snifer:
- 1 - Introducción a Obsidian : https://www.youtube.com/watch?v=-U4PwvV1DwY
- 2 - Aprendiendo Markdown : https://youtu.be/QQIZ7XUdXSc
- 3 - Instalación y configuración de Obsidian : https://www.youtube.com/watch?v=GWRx_e1bWwA
- 4 - Aprendiendo a instalar Temas y Plugins en Obsidian : https://www.youtube.com/watch?v=W9U7TfABVSo
- 5 - Tratamiento de ficheros multimedia (PDF, Imágenes, Audio, Vídeos) en Obsidian : https://www.youtube.com/watch?v=FlEz8QJHrss  
- 6 - Como convertir un fichero PDF a Markdown - Como instalar plug-ins no listados en Obsidian con BRAT : https://www.youtube.com/watch?v=dHWZOyYmgcs
- 7 - Planificación de proyectos y Calendario : https://www.youtube.com/watch?v=Obz7JVYfeaY&t=37s
- 8- Cómo realizar búsqueda de contenido en Obsidian: https://www.youtube.com/watch?v=Pr2UQHw8dy4
- Tu Sistema de Notas en Obsidian #zettelkasten  (Basado en plantilla de Ruben Loan) : https://youtu.be/8UoIJsx2NuA

[[index]]