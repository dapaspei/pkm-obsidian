# PKM Obisdian Mastermind

## Nota de Introducción y Referencias del grupo PKM con Obsidian
Autor::Alex
Version:: 2022.06.19

¡Bienvenid@!

Se te ha invitado a participar en este grupo a partir de tu declaración de intenciones de participar en el Mastermind del Grupo Telegram PKM_es (https://t.me/PKM_es), cuya temática de trabajo será el desarrollo del PKM con Obsidian. 

Entre los temas que esperamos tengan cabida y desarrollo en este grupo están las diferentes formas de implementar con Obsidian las metodologías asociadas al PKM: 
- GTD
- BASB (especialmente PARA y Resumen Progresivo)
- Zettelkasten
- ...
  
También intentaremos trabajar en Obsidian otros temas asociados al PKM, como: 
- Gestión del Diario
- Búsquedas de Información
- Etiquetados y Conexiones
- Uso y valoración de los plugins
- Uso de la app móvil
- Flujos de Trabajo y automatizaciones
- Backups y Copias de Seguridad de la Información

Iniciamos en paralelo la lista de #etiquetas (https://t.me/c/1625133683/276) [[Temas]]  que vayamos descubriendo y considerando como relacionadas con este proyecto de investigación colectiva, que también se marcará como mensaje fijado de este canal.

[[Vídeos]]

[[Eventos]]



