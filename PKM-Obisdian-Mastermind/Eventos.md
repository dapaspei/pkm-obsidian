# Eventos
Autor::Alex
Versión:: 2022.06.19

Relación de eventos programados y celebrados hasta la fecha en este grupo Telegram:

- 2022/04/30: Nace el grupo Telegram PKM con Obsidian.
- 2022/05/10: 1a. Multivideoconferencia de Arranque del Mastermind (a las 19:30): Presentaciones y brainstorming temas a desarrollar. 
- 2022/05/17: Mutivideoconferencia con Xoan Hermelo, a las 19:00, donde nos presentará cómo usa Obsidian en su PKM.
- 2022/06/10: 2a. Multivideoconferencia del Mastermind (a las 19:30): Nuevas incorporaciones; continuación del trabajo sobre la estructura organizativa de los PKM's y nuestros contenedores de información.
- 2022/09/xx: En la 2a. quincena de septiembre retomaremos la actividad del mastermind

[[index]]